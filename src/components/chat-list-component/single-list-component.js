import React from 'react';

const SingleListItem = (props) => {
	return (
        <div>
			<span>Status: {props.status}</span>
			<p>
				{props.name}
			</p>
        </div>
    );
};

export default SingleListItem;
