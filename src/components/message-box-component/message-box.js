import React from 'react';
import './message-box.css';
import { useState } from 'react';
import useGlobal from "../../store";
import FaPaperPlane from  "react-icons/lib/fa/paper-plane-o";




const MessageBox = ({currentUser}) => {

    const [message, setMessage] = useState("");
    
    const [globalState, globalActions] = useGlobal();
    
    

    const sendMessage = () => {
        globalActions.addMessaje(message, currentUser.id);
        setMessage("");
        
    }

    

    const keyPressed = (event) => {
        globalActions.isTyping(true, currentUser);
        
        if (event.key === "Enter") {
            sendMessage();
            
        }
        setTimeout(()=>{
            globalActions.isTyping(false, currentUser);
        },5000)
    }
        

	return (
        <div className="message-box-component">
            <textarea value={message}  onKeyPress={keyPressed}   onChange={e => setMessage(e.target.value)} >
            </textarea>
            <FaPaperPlane className="send-icon" onClick={sendMessage} />
        </div>
    );
};

export default MessageBox;
