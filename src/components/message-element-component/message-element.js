import React from 'react';
import './message-element.css';
import useGlobal from "../../store";
import { useState, useEffect } from 'react';
import FaCheck from 'react-icons/lib/fa/check';

// import { Msg } from '../message-component/message';

const Msg = ({ message, time }) => {

    return (
        <p className="message">
            <span>{message}</span>  {time}
        </p>
    );
};


const MessageElement = ({ messageItem, currentId }) => {

    const [globalState, globalActions] = useGlobal();

    const [messageClass, setMessageClass] = useState('current-user');
    const [last, setLast] = useState('show');
    const [margin, setMargin] = useState('none');
    const [elId, setMessageId] = useState(messageItem.id);
    const [el, setMessage] = useState(messageItem);

    const scrollToEnd = () =>{
        var container = document.getElementById("chat-component");
        container.scrollTop = container.scrollHeight;
    }

    useEffect(() => {
        if (globalState[currentId].currentUser.id != el.userId) {
            setMessageClass('friend-user');
            globalActions.setRevicievedMessage(globalState[currentId].currentUser.id, elId);
        }
        if (globalState.conversationArray.length > 1) {
            // debugger;
            if (el.hide) {
                setLast('hide');
                setMargin('set');
            } else {
                setLast('show');
                setMargin('none');
            }
        }
        scrollToEnd();
    }, [])
   
    return (
        <div className={`messaage-element-containter ${messageClass}`}>
            <div className={`profile-picture ${last}`}>
                <img src={require(`../../assets/${el.img}`)} />
            </div>
            <div className="message-data">
                <div className={`message-info ${last}`}>
                    <div className="message-profile-info">            
                        {el.username}:
                    </div>
                </div>
                <div className={`message ${margin}`}>
                    <div className="text">
                        {el.message}
                    </div> 
                    {globalState[currentId].currentUser.id == el.userId && el.sent && <FaCheck className="icon"/>}
                    {globalState[currentId].currentUser.id == el.userId && el.recieved && <FaCheck className="icon last" />}
                    <div className="time">{el.time}</div> 
                </div>
                
            </div>
        </div>
    );
};

export default MessageElement;
