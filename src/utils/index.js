import React from "react";

export const renderTime = () => {
  const currentTime = new Date().toLocaleTimeString('en-US', {hour: '2-digit', minute:'2-digit', hour12: true });

  // let parsedTime = currentTime.replace(/:\d+ /, ' ');

  return (
    <div>{currentTime}</div>
  );
};
