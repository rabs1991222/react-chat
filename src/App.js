import React from 'react';
import './App.css';
import ChatView from './components/chat-view/chat-view';
import { useState, useEffect } from 'react';
import useGlobal from "./store";
import { setTimeout } from 'timers';

const App = () => {

	const [globalState, globalActions] = useGlobal();
	const [robInfo, setRobInfo] = useState({
		id:'userA',
		img:'img.png', 
		username:'Rob',
		name:'Roberto Barrios',
		isTyping:false,
		status:false,
		messages :[]
	});

	const [lauInfo, setLauInfo] = useState({
		id:'userB',
		img:'img.png', 
		username:'Lau',
		name:'Laura Josh',
		isTyping:false,
		status:false,
		messages :[]
	});

	return (
		<div className="root">
			<ChatView currentUser={robInfo} chatUser={lauInfo} />
			<ChatView currentUser={lauInfo} chatUser={robInfo} />
		</div>
	);
};

export default App;
